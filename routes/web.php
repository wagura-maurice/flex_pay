<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use App\Helpers\LP;
use App\Helpers\Rave;
use App\Helpers\Mpesa;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/v1'], function($app) {
	// Routes for payments
	$app->group(['prefix' => 'payment'], function($app) {
		/**
		 * Routes for resource currency
		 */
		$app->get('currency', 'PaymentCurrencyController@index');
		$app->get('currency/{id}', 'PaymentCurrencyController@show');
		$app->post('currency', 'PaymentCurrencyController@create');
		$app->put('currency/{id}', 'PaymentCurrencyController@update');
		$app->delete('currency/{id}', 'PaymentCurrencyController@delete');
		/**
		 * Routes for resource config
		 */
		$app->get('config', 'PaymentConfigController@index');
		$app->get('config/{id}', 'PaymentConfigController@show');
		$app->post('config', 'PaymentConfigController@create');
		$app->put('config/{id}', 'PaymentConfigController@update');
		$app->delete('config/{id}', 'PaymentConfigController@delete');
		/**
		 * Routes for resource method
		 */
		$app->get('method', 'PaymentMethodController@index');
		$app->get('method/{id}', 'PaymentMethodController@show');
		$app->post('method', 'PaymentMethodController@create');
		$app->put('method/{id}', 'PaymentMethodController@update');
		$app->delete('method/{id}', 'PaymentMethodController@delete');
	});
	// Routes for money
	$app->group(['prefix' => 'money'], function($app) {
		/**
		 * Routes for resource money in
		 */
		$app->get('in', 'MoneyInController@index');
		$app->get('in/{id}', 'MoneyInController@show');
		$app->post('in', 'MoneyInController@create');
		$app->put('in/{id}', 'MoneyInController@update');
		$app->delete('in/{id}', 'MoneyInController@delete');
		/**
		 * Routes for resource money out
		 */
		$app->get('out', 'MoneyOutController@index');
		$app->get('out/{id}', 'MoneyOutController@show');
		$app->post('out', 'MoneyOutController@create');
		$app->put('out/{id}', 'MoneyOutController@update');
		$app->delete('out/{id}', 'MoneyOutController@delete');
	});

	/**
	 * Routes for resource invoice
	 */
	$app->get('invoice', 'InvoiceController@index');
	$app->get('invoice/{id}', 'InvoiceController@show');
	$app->post('invoice', 'InvoiceController@create');
	$app->put('invoice/{id}', 'InvoiceController@update');
	$app->delete('invoice/{id}', 'InvoiceController@delete');

	// rave resource
	$app->group(['prefix' => 'rave'], function($app) {

		$app->post('endpoint', function (Request $request) use ($app) {
			return LP::respond(Response::HTTP_CREATED, json_decode(Rave::endpoint($request), true));
		});

		$app->post('callback', function (Request $request) use ($app) {
			return Rave::callback($request);
		});
	});

	// mpesa resource
	$app->group(['prefix' => 'mpesa'], function($app) {
		$app->get('access_token', function () use ($app) {
			return Mpesa::generateAccessToken();
		});
		$app->get('registerurl', function () use ($app) {
			return Mpesa::registerRoute();
		});
		/**
		 * Routes for C2B mpesa resource
		 */
		$app->group(['prefix' => 'c2b'], function($app) {
			$app->post('endpoint', function (Request $request) use ($app) {
				return Mpesa::C2B_endpoint($request);
			}); // simulate c2b payment transaction
			$app->post('validation', function (Request $request) use ($app) {
				return Mpesa::C2B_validation($request);
			}); // validate payment
			$app->post('confirmation', function (Request $request) use ($app) {
				return Mpesa::C2B_confirmation($request);
			}); // confirm the transaction
		});
		/**
		 * Routes for B2C mpesa resource
		 */
		$app->group(['prefix' => 'b2c'], function($app) {
			$app->post('endpoint', function (Request $request) use ($app) {
				return Mpesa::B2C_endpoint($request);
			});
			$app->post('timeout', function (Request $request) use ($app) {
				return Mpesa::B2C_timeout($request);
			});
			$app->post('callback', function (Request $request) use ($app) {
				return Mpesa::B2C_callback($request);
			});
		});
		/**
		 * Routes for mpesa express resource
		 */
		$app->group(['prefix' => 'express'], function($app) {
			$app->post('endpoint', function (Request $request) use ($app) {
				return Mpesa::Express_endpoint($request);
			}); // simulate push stk payment transaction
			$app->post('callback', function (Request $request) use ($app) {
				return Mpesa::Express_callback($request);
			}); // simulate push stk payment callback
		});
	});

});
