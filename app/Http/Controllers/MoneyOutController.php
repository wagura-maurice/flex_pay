<?php
 
namespace App\Http\Controllers;

use \App\Helpers\LP;
use App\MoneyOut;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
 
class MoneyOutController extends Controller {

    public function index() {
 
        return LP::respond(Response::HTTP_OK, MoneyOut::all());
    }

    public function create(Request $request) {

        self::validate($request, MoneyOut::$create_rules);

        return LP::respond(Response::HTTP_CREATED, MoneyOut::create($request->all()));
    }

    public function show($id) {
 
        $money_out = MoneyOut::find($id);

        return is_null($money_out) ? LP::respond(Response::HTTP_NOT_FOUND) : LP::respond(Response::HTTP_OK, $money_out);
 
    }
 
    public function update(Request $request, $id) {

        $money_out = MoneyOut::find($id);

        if(is_null($money_out)) {
            return LP::respond(Response::HTTP_NOT_FOUND);
        }

        self::validate($request, MoneyOut::$update_rules);
        $money_out->update($request->all());

        return LP::respond(Response::HTTP_OK, $money_out);
    }  

    public function delete($id) {

        $money_out = MoneyOut::find($id);

        return is_null($money_out) ? LP::respond(Response::HTTP_NOT_FOUND) : LP::respond(Response::HTTP_NO_CONTENT, $money_out->destroy($id));
    }
}
?>