<?php
 
namespace App\Http\Controllers;

use \App\Helpers\LP;
use App\PaymentMethod;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
 
class PaymentMethodController extends Controller {

    public function index() {
 
        return LP::respond(Response::HTTP_OK, PaymentMethod::all());
    }

    public function create(Request $request) {

        self::validate($request, PaymentMethod::$create_rules);

        return LP::respond(Response::HTTP_CREATED, PaymentMethod::create($request->all()));
    }

    public function show($id) {
 
        $payment_method = PaymentMethod::find($id);

        return is_null($payment_method) ? LP::respond(Response::HTTP_NOT_FOUND) : LP::respond(Response::HTTP_OK, $payment_method);
 
    }
 
    public function update(Request $request, $id) {

        $payment_method = PaymentMethod::find($id);

        if(is_null($payment_method)) {
            return LP::respond(Response::HTTP_NOT_FOUND);
        }

        self::validate($request, PaymentMethod::$update_rules);
        
        $payment_method->update($request->all());

        return LP::respond(Response::HTTP_OK, $payment_method);
    }  

    public function delete($id) {

        $payment_method = PaymentMethod::find($id);

        return is_null($payment_method) ? LP::respond(Response::HTTP_NOT_FOUND) : LP::respond(Response::HTTP_NO_CONTENT, $payment_method->destroy($id));
    }
}
?>