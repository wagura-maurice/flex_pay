<?php
 
namespace App\Http\Controllers;

use \App\Helpers\LP;
use App\MoneyIn;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
 
class MoneyInController extends Controller {

    public function index() {
 
        return LP::respond(Response::HTTP_OK, MoneyIn::all());
    }

    public function create(Request $request) {

        self::validate($request, MoneyIn::$create_rules);

        return LP::respond(Response::HTTP_CREATED, MoneyIn::create($request->all()));
    }

    public function show($id) {
 
        $money_in = MoneyIn::find($id);

        return is_null($money_in) ? LP::respond(Response::HTTP_NOT_FOUND) : LP::respond(Response::HTTP_OK, $money_in);
 
    }
 
    public function update(Request $request, $id) {

        $money_in = MoneyIn::find($id);

        if(is_null($money_in)) {
            return LP::respond(Response::HTTP_NOT_FOUND);
        }

        self::validate($request, MoneyIn::$update_rules);
        $money_in->update($request->all());

        return LP::respond(Response::HTTP_OK, $money_in);
    }  

    public function delete($id) {

        $money_in = MoneyIn::find($id);

        return is_null($money_in) ? LP::respond(Response::HTTP_NOT_FOUND) : LP::respond(Response::HTTP_NO_CONTENT, $money_in->destroy($id));
    }
}
?>