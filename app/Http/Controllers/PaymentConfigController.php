<?php
 
namespace App\Http\Controllers;

use \App\Helpers\LP;
use App\PaymentConfig;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
 
class PaymentConfigController extends Controller {

    public function index() {
 
        return LP::respond(Response::HTTP_OK, PaymentConfig::all());
    }

    public function create(Request $request) {

        self::validate($request, PaymentConfig::$create_rules);

        return LP::respond(Response::HTTP_CREATED, PaymentConfig::create($request->all()));
    }

    public function show($id) {
 
        $payment_config = PaymentConfig::find($id);

        return is_null($payment_config) ? LP::respond(Response::HTTP_NOT_FOUND) : LP::respond(Response::HTTP_OK, $payment_config);
 
    }
 
    public function update(Request $request, $id) {

        $payment_config = PaymentConfig::find($id);

        if(is_null($payment_config)) {
            return LP::respond(Response::HTTP_NOT_FOUND);
        }

        self::validate($request, PaymentConfig::$update_rules);
        $payment_config->update($request->all());

        return LP::respond(Response::HTTP_OK, $payment_config);
    }  

    public function delete($id) {

        $payment_config = PaymentConfig::find($id);

        return is_null($payment_config) ? LP::respond(Response::HTTP_NOT_FOUND) : LP::respond(Response::HTTP_NO_CONTENT, $payment_config->destroy($id));
    }
}
?>