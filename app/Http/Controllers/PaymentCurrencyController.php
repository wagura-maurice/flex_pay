<?php
 
namespace App\Http\Controllers;

use \App\Helpers\LP;
use App\PaymentCurrency;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PaymentCurrencyController extends Controller {

    public function index() {
 
        return LP::respond(Response::HTTP_OK, PaymentCurrency::all());
    }

    public function create(Request $request) {

        self::validate($request, PaymentCurrency::$create_rules);

        return LP::respond(Response::HTTP_CREATED, PaymentCurrency::create($request->all()));
    }

    public function show($id) {
 
        $payment_currency = PaymentCurrency::find($id);

        return is_null($payment_currency) ? LP::respond(Response::HTTP_NOT_FOUND) : LP::respond(Response::HTTP_OK, $payment_currency);
 
    }
 
    public function update(Request $request, $id) {

        $payment_currency = PaymentCurrency::find($id);

        if(is_null($payment_currency)) {
            return LP::respond(Response::HTTP_NOT_FOUND);
        }

        self::validate($request, PaymentCurrency::$update_rules);
        $payment_currency->update($request->all());

        return LP::respond(Response::HTTP_OK, $payment_currency);
    }  

    public function delete($id) {

        $payment_currency = PaymentCurrency::find($id);

        return is_null($payment_currency) ? LP::respond(Response::HTTP_NOT_FOUND) : LP::respond(Response::HTTP_NO_CONTENT, $payment_currency->destroy($id));
    }
}
?>