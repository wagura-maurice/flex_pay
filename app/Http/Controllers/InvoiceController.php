<?php
 
namespace App\Http\Controllers;

use App\Invoice;
use App\Helpers\LP;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
 
class InvoiceController extends Controller {

    public function index() {
 
        return LP::respond(Response::HTTP_OK, Invoice::all());
    }

	public function create(Request $request) {

        $request['invoice_number'] = LP::generateInvoiceNumber();

        self::validate($request, Invoice::$create_rules);

        return LP::respond(Response::HTTP_CREATED, Invoice::create($request->all()));
	}

    public function show($id) {
 
        $invoice = Invoice::find($id);

        return is_null($invoice) ? LP::respond(Response::HTTP_NOT_FOUND) : LP::respond(Response::HTTP_OK, $invoice);
 
    }
 
	public function update(Request $request, $id) {

        $invoice = Invoice::find($id);

        if(is_null($invoice)) {
            return LP::respond(Response::HTTP_NOT_FOUND);
        }

        self::validate($request, Invoice::$update_rules);
        $invoice->update($request->all());

        return LP::respond(Response::HTTP_OK, $invoice);
	}  

	public function delete($id) {

        $invoice = Invoice::find($id);

        return is_null($invoice) ? LP::respond(Response::HTTP_NOT_FOUND) : LP::respond(Response::HTTP_NO_CONTENT, $invoice->destroy($id));
 	}
}
?>