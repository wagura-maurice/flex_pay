<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentCurrency extends Model {

	protected $table = 'lp_payment_currency';

    protected $fillable = [
    	'currency_name',
    	'currency_code',
    	'exchange_rate',
    	'payment_currency_status'
    ];

    public static $create_rules = [
        'currency_name' => 'required|string|unique:lp_payment_currency',
        'currency_code' => 'required|string|unique:lp_payment_currency',
        'exchange_rate' => 'required|string',
    ];

    public static $update_rules = [
        'currency_name' => 'required|string',
        'currency_code' => 'required|string',
        'exchange_rate' => 'required|string',
    ];

}
