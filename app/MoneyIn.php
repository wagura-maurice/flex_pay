<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MoneyIn extends Model {

    protected $table = 'lp_money_in';

    protected $date = 'transaction_date';

    protected $fillable = [
        'names',
        'email',
        'phone',
        'transaction_amount',
        'transaction_code',
        'transaction_date',
    	'booking_reference',
    	'payment_method_id',
        'payment_details',
        'request_id',
    	'money_in_status'
    ];

    public static $create_rules = [
        'names' => 'required|string',
        'amount' => 'required|regex:/^\d*(\.\d{2})?$/',
        'transaction_code' => 'required|string|unique:lp_money_in',
        'transaction_date' => 'required|timestamp',
		'booking_reference' => 'required|string',
		'payment_method_id' => 'required|numeric',
        'payment_details' => 'required|string'
    ];

    public static $update_rules = [
        'names' => 'required|string',
        'amount' => 'required|regex:/^\d*(\.\d{2})?$/',
        'transaction_code' => 'required|string',
        'transaction_date' => 'required|timestamp',
        'booking_reference' => 'required|string',
        'payment_method_id' => 'required|numeric',
        'payment_details' => 'required|string'
    ];

}
