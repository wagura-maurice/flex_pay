<?php

namespace App\Helpers;

use App\MoneyIn;
use App\Helpers\LP;

class Rave {


	public static function endpoint($request) {

		$txref = $request->booking_reference ? $request->booking_reference : LP::generateBookingReference();

		$url = LP::getConfig('Rave_EndPoint');

		$data = json_encode([
			'amount' => $request->amount,
			'currency' => $request->currency,
			'country' => $request->country,
			'customer_email' => $request->customer_email,
			'customer_phone' => $request->customer_phone,
			'customer_firstname' => $request->customer_firstname,
			'customer_lastname' => $request->customer_lastname,
			'custom_title' => 'FlexPay',
			'custom_description' => 'Lipa Pole Pole',
			'custom_logo' => 'https://www.flexpay.co.ke/assets/images/flexpay-logo.svg',
			'txref' => $txref,
			'PBFPubKey' => LP::getConfig('Rave_PublicKey'),
			'redirect_url' => LP::getConfig('Rave_Callback'),
			'payment_options' => 'card',
			'payment_plan' => 'pass the plan id'
		]);

		$headers = json_encode(array(
			'Content-Type: application/json',
			'Cache-Control: no-cache',
		));

		$transaction = json_decode(LP::CURL_POST($url, $data, $headers));

		if(!$transaction->data && !$transaction->data->link){
			// there was an error from the API
			return json_encode([
				'rave_status' => $transaction->status,
				'rave_txref' => $txref,
				'rave_message' => $transaction->message
			]);
		} else {
			return json_encode([
				'rave_status' => $transaction->status,
				'rave_txref' => $txref,
				'rave_message' => $transaction->message,
				'rave_link' => $transaction->data->link
			]);
		}
	}

	public static function callback($request) {

		$transaction = json_decode($request->resp)->data;

		$url = 'https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/xrequery';

		$data = json_encode(array(
			"SECKEY" => LP::getConfig('Rave_PrivateKey'),
			"txref" => $transaction->data->txRef,
			"include_payment_entity" => "1"
		));

		$headers = json_encode(array(
			'Content-Type: application/json',
			'Cache-Control: no-cache',
		));

		$resp = json_decode(LP::CURL_POST($url, $data, $headers));

		// $paymentStatus = $resp['data']['status'];

		if (($resp->data->chargecode == "00" || $resp->data->chargecode == "0") && ($resp->data->amount == $transaction->data->amount)  && ($resp->data->currency == $transaction->data->currency)) {
			// transaction was successful...
			// please check other things like whether you already gave value for this ref
			// if the email matches the customer who owns the product etc
			// Give Value and return to Success page
			$money_in = MoneyIn::create([
				'names' => $resp->data->custname,
				'email' => $resp->data->custemail,
				'phone' => $resp->data->custphone,
				'transaction_amount' => $resp->data->amount,
				'transaction_code' => $resp->data->txid,
				'transaction_date' => \Carbon\Carbon::parse($resp->data->created)->format('Y-m-d H:i:s'),
				'booking_reference' => $resp->data->txref ? $resp->data->txref : LP::generateBookingReference(),
				'payment_method_id' => 2, // rave
				'payment_details' => json_encode($resp)
			]);

			LP::NOTIFY(LP::suffix($resp->data->custphone), 'Dear customer, card transaction of ' . number_format($resp->data->amount, 2) . ' successfully paid');
			
			return json_encode(['status' => 'success', 'data' => $money_in]);
			
		} else {
			// Dont Give Value and return to Failure page

			LP::NOTIFY(LP::suffix($resp->data->custphone), 'Dear customer, card transaction of ' . number_format($resp->data->amount, 2) . ' failed, please try again later.');

			return json_encode(['status' => 'failed']);
		}
	}
}
