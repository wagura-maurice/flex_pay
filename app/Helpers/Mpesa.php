<?php

namespace App\Helpers;

use App\MoneyIn;
use App\Helpers\LP;

use Carbon\Carbon;
use Log;

class Mpesa {

	public static function generateAccessToken() {
		
		$url = 'https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

		$curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, [
			'Authorization: Basic ' . base64_encode(LP::getConfig('Mpesa_ConsumerKey') . ':' . LP::getConfig('Mpesa_ConsumerSecret')),
			'Content-Type: application/json'
		]); //setting a custom header
		curl_setopt($curl, CURLOPT_HEADER, false);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

		$curl_response = curl_exec($curl);

		$PaymentConfig = \App\PaymentConfig::where(['name' => 'Mpesa_ClientCredentials'])->first();
		$PaymentConfig->value = trim($curl_response);
		$PaymentConfig->save();

		return json_decode($curl_response, true);

	}

	public static function registerRoute() {

		$url = 'https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl';

		$data = json_encode([
			'ShortCode' => LP::getConfig('Mpesa_ShortCode'),
			'ResponseType' => 'completed',
			'ValidationURL' => 'https://892c360f.ngrok.io/api/v1/mpesa/c2b/validation',
			'ConfirmationURL' => 'https://892c360f.ngrok.io/api/v1/mpesa/c2b/confirmation'
		]);

		$headers = json_encode(array(
			'Content-Type: application/json',
			'Cache-Control: no-cache',
			'Authorization: Bearer ' . json_decode(LP::getConfig('Mpesa_ClientCredentials'))->access_token,
		));

		return json_decode(LP::CURL_POST($url, $data, $headers), true);
	}

	public static function C2B_endpoint($request) {

		$url = 'https://sandbox.safaricom.co.ke/mpesa/c2b/v1/simulate';

		$data = json_encode([
			'ShortCode' => LP::getConfig('Mpesa_ShortCode'),
			'CommandID' => 'CustomerPayBillOnline',
			'Amount' => $request->amount,
			'Msisdn' => LP::suffix($request->phone),
			'BillRefNumber' => $request->booking_reference ? $request->booking_reference : LP::generateBookingReference()
		]);

		$headers = json_encode(array(
			'Content-Type: application/json',
			'Cache-Control: no-cache',
			'Authorization: Bearer ' . json_decode(LP::getConfig('Mpesa_ClientCredentials'))->access_token,
		));

		$transaction = json_decode(LP::CURL_POST($url, $data, $headers), true);

		return $transaction;

	}

    public static function C2B_validation($request) {

        $data = (object) $request;
        $transaction_id = Carbon::now()->format('ymdis');

        $money_in = MoneyIn::create([
            'names' => $data->FirstName ." ". $data->MiddleName ." ". $data->LastName,
            'phone' => $data->MSISDN,
            'transaction_amount' => $data->TransAmount,
            'transaction_code' => $data->TransID,
            'transaction_date' => $data->TransTime,
            'booking_reference' => $data->BillRefNumber ? $data->BillRefNumber : LP::generateBookingReference(),
            'payment_method_id' => 1, // mpesa
            'payment_details' => json_encode([
                'business_short_code' => $data->BusinessShortCode,
                'third_party' => $transaction_id,
                'org_account_balance' => $data->OrgAccountBalance
            ])
        ]);

        if ($money_in) {
			LP::NOTIFY(LP::suffix($money_in['phone']), 'Dear ' . ucwords($money_in['names']) . ', mpesa transaction of ' . number_format($money_in['transaction_amount'], 2) . ' successfully paid.');
            // tell safaricom that you've accepted the payment
            return response()->json([
                "ResultCode" => 0,
                "ResultDesc" => "Payment Accepted",
                "ThirdPartyTransID" => $transaction_id
            ]);
        } else {
			LP::NOTIFY(LP::suffix($money_in['phone']), 'Dear ' . ucwords($money_in['names']) . ', mpesa transaction of ' . number_format($money_in['transaction_amount'], 2) . ' failed, please try again later.');
            // tell safaricom that you've rejected the payment
            return response()->json([
                "ResultCode" => 1,
                "ResultDesc" => "Payment Rejected",
                "ThirdPartyTransID" => $transaction_id
            ]);
        }
    }

    public static function C2B_confirmation($request) {

        $data = (object) $request;
        $update = MoneyIn::where('transaction_code', $data->TransID);
        $update->update(['money_in_status' => 'success']);
        
        return ;
	}
	
	public static function B2C_endpoint($request) {

		$url = 'https://sandbox.safaricom.co.ke/mpesa/b2c/v1/paymentrequest';

		$data = json_encode([
			'InitiatorName' => LP::getConfig('Mpesa_InitiatorName'),
			'SecurityCredential' => LP::getConfig('Mpesa_SecurityCredential'),
			'CommandID' => 'BusinessPayment',
			'Amount' => $request->amount,
			'PartyA' => '603061', // LP::getConfig('Mpesa_ShortCode'),
			'PartyB' => LP::suffix($request->phone),
			'Remarks' => $request->remarks ? $request->remarks : 'flexpay-B2C',
			'QueueTimeOutURL' => 'https://892c360f.ngrok.io/api/v1/mpesa/b2c/timeout',
			'ResultURL' => 'https://892c360f.ngrok.io/api/v1/mpesa/b2c/callback',
			'Occasion' => $request->occasion ? $request->occasion : '12'
		]);

		$headers = json_encode(array(
			'Content-Type: application/json',
			'Cache-Control: no-cache',
			'Authorization: Bearer ' . json_decode(LP::getConfig('Mpesa_ClientCredentials'))->access_token,
		));

		$transaction = json_decode(LP::CURL_POST($url, $data, $headers), true);

		return $transaction;

	}

	public static function B2C_timeout($request) {
		Log::info("B2C TIMEOUT");
        Log::info(print_r($request->all(),true));

	}

	public static function B2C_callback($request) {
		Log::info("B2C Callback");
        Log::info(print_r($request->all(),true));

	}

	public static function Express_endpoint($request) {

		$timestamp = Carbon::now()->format('YmdHis');
		$url = 'https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest';

		$booking_reference = $request->booking_reference ? $request->booking_reference : LP::generateBookingReference();

		$data = json_encode(array(
			'BusinessShortCode' => LP::getConfig('Express_ShortCode'),
			'Password' => LP::getPassword(LP::getConfig('Express_ShortCode'), LP::getConfig('Mpesa_PassKey'), $timestamp),
			'Timestamp' => $timestamp,
			'TransactionType' => 'CustomerPayBillOnline',
			'Amount' => $request->amount,
			'PartyA' => LP::suffix($request->phone),
			'PartyB' => LP::getConfig('Express_ShortCode'),
			'PhoneNumber' => LP::suffix($request->phone),
			'CallBackURL' => 'https://892c360f.ngrok.io/api/v1/mpesa/express/callback',
			'AccountReference' => $booking_reference,
			'TransactionDesc' => $request->description
		));

		$headers = json_encode(array(
			'Content-Type: application/json',
			'Cache-Control: no-cache',
			'Authorization: Bearer ' . json_decode(LP::getConfig('Mpesa_ClientCredentials'))->access_token,
		));

		$transaction = json_decode(LP::CURL_POST($url, $data, $headers), true);

		if (isset($transaction['ResponseCode']) && $transaction['ResponseCode'] == 0) {
			MoneyIn::create([
				'transaction_amount' => $request->amount,
				'transaction_code' => ' ',
				'transaction_date' => $timestamp,
				'payment_method_id' => 1,
				'request_id' => $transaction['CheckoutRequestID']
			]);
		}

		return $transaction;

	}

	public static function Express_callback($request) {
		
		$transaction = $request['Body']['stkCallback'];
		
		if ($transaction['ResultCode'] == 0) {
			
			$data = [
				'phone' => $transaction['CallbackMetadata']['Item'][4]['Value'],
				'transaction_amount' => $transaction['CallbackMetadata']['Item'][0]['Value'],
				'transaction_code' => $transaction['CallbackMetadata']['Item'][1]['Value'],
				'transaction_date' => $transaction['CallbackMetadata']['Item'][3]['Value'],
				'payment_method_id' => 1, // mpesa
				'payment_details' => json_encode($transaction),
				'money_in_status' => 'success'
			];

			$money_in = MoneyIn::where('request_id', $transaction['CheckoutRequestID']);
			$money_in->update($data);

			$notification = LP::NOTIFY(LP::suffix($data['phone']), 'Dear customer, mpesa transaction of ' . number_format($data['transaction_amount'], 2) . ' successfully paid.');

		}
        
        return ;

	}

}
