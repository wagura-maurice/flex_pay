<?php

namespace App\Helpers;


class LP {

    public static function CURL_POST($url, $data, $headers = '') {

		$curl = curl_init();

		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, json_decode($headers));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

		$curl_response = curl_exec($curl);
		$err = curl_error($curl);

		return $err ? $err : $curl_response;

    }

    public static function NOTIFY($phone, $message) {
        $url = 'http://127.0.0.1:3000/api/v1/sms/papohapo';

		$data = json_encode([
			'to' => $phone,
			'message' => $message
        ]);

        $headers = json_encode(array(
			'Content-Type: application/json',
			'Cache-Control: no-cache',
		));
        
        $notification = json_decode(LP::CURL_POST($url, $data, $headers));
    }
    
    public static function generateInvoiceNumber() {
        $invoice = \App\Invoice::whereYear('created_at', '=', date('Y'))->orderBy('id', 'DESC');
        $number = !is_null($invoice->first()) ? substr($invoice->first()->id, 6) : '0';

        return $invoiceNumber = date('Y') . sprintf("%'.06d\n", ($number + !is_null($invoice->first()) ? $invoice->first()->id : '0'));
    }

    public static function getConfig($name) {
        return \App\PaymentConfig::where(['name' => $name, 'payment_config_status' => 'active'])->first()->value;
    }

    public static function suffix($phone) {
        return Self::getConfig("CountryCode") . substr($phone, -9);
    }

    public static function generateBookingReference() {
        return "flexpay - " . rand();
	}
	
	public static function getPassword($shortCode, $passkey, $time) {
        return base64_encode($shortCode . $passkey . $time);
    }

	public static function appURL() {
		return !is_null(env('APP_URL')) ? env('APP_URL') : 'http://127.0.0.1:8000';
	}

	public static function respond($status, $data = []) {
        return response()->json($data, $status);
    }
}
