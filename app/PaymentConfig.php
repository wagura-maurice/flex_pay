<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentConfig extends Model {

	protected $table = 'lp_payment_config';

    protected $fillable = [
    	'name',
    	'value',
    	'payment_config_status'
    ];

    public static $create_rules = [
        'name' => 'required|string|unique:lp_payment_config',
        'value' => 'nullable|string'
    ];

    public static $update_rules = [
        'name' => 'required|string',
        'value' => 'nullable|string'
    ];

}
