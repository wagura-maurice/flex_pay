<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model {

	protected $table = 'lp_payment_method';

    protected $fillable = [
    	'name',
    	'account_number',
    	'description',
    	'payment_method_status'
    ];

    public static $create_rules = [
        'name' => 'required|string|unique:lp_payment_method',
        'account_number' => 'required|string|unique:lp_payment_method'
    ];

    public static $update_rules = [
        'name' => 'required|string',
        'account_number' => 'required|string'
    ];

}
