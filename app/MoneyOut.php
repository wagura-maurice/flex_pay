<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MoneyOut extends Model {

    protected $table = 'lp_money_out';

    protected $date = 'transaction_date';

    protected $fillable = [
        'user_id' ,
    	'amount',
    	'transaction_code',
        'transaction_date',
    	'payment_method_id',
    	'money_out_status'
    ];

    public static $create_rules = [
        'user_id' => 'required|numeric',
        'amount' => 'required|regex:/^\d*(\.\d{2})?$/',
        'transaction_code' => 'required|string|unique:lp_money_out',
        'transaction_date' => 'required|timestamp',
    	'payment_method_id'=> 'required|numeric'
    ];

    public static $update_rules = [
        'user_id' => 'required|numeric',
        'amount' => 'required|regex:/^\d*(\.\d{2})?$/',
        'transaction_code' => 'required|string',
        'transaction_date' => 'required|timestamp',
        'payment_method_id'=> 'required|numeric'
    ];

}
