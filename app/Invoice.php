<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model {

    protected $table = 'lp_invoice';

    protected $date = 'due';

    protected $fillable = [
    	'invoice_number',
    	'amount',
    	'account_number',
    	'user_id' ,
    	'invoice_status',
        'due'
    ];

    public static $create_rules = [
        'invoice_number' => 'required|unique:lp_invoice',
        'amount' => 'required|regex:/^\d*(\.\d{2})?$/',
    	'account_number'=> 'required|string',
    	'user_id' => 'required|numeric',
        'due' => 'required|date'
    ];

    public static $update_rules = [
        'invoice_number' => 'required|unique:lp_invoice',
        'amount' => 'required|regex:/^\d*(\.\d{2})?$/',
        'account_number'=> 'required|string',
        'user_id' => 'required|numeric',
        'due' => 'required|date'
    ];

}
