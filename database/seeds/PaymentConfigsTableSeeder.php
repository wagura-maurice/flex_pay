<?php

use App\PaymentConfig;
use Illuminate\Database\Seeder;

class PaymentConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        PaymentConfig::unguard();

        DB::table('lp_payment_config')->delete();

        $PaymentConfigs = array(
            ['name' => 'Mpesa_ConsumerKey', 'value' => 'AnkXGAu7AxWZMOGYDzVji9xOvG7TI5GN', 'payment_config_status' => 'active'],
            ['name' => 'Mpesa_ConsumerSecret', 'value' => '5oAYPeqxTuHf95hD', 'payment_config_status' => 'active'],
            ['name' => 'Mpesa_ShortCode', 'value' => '600000', 'payment_config_status' => 'active'],
            ['name' => 'Express_ShortCode', 'value' => '174379', 'payment_config_status' => 'active'],
            ['name' => 'Mpesa_InitiatorName', 'value' => 'safaricom.8', 'payment_config_status' => 'active'],
            ['name' => 'Mpesa_SecurityCredential', 'value' => 'gfAO/MehyBbv9h8eBK5y7PKLPfTfkGFKX1B8zL2IM/4M5d90j04kpMmReS/IPOaDAOv7me3YbdkznIYACuamhBzdlDgVdX2GM73ssz4u5yRN4RkIbp9PoTMhrwBS3m7rmAcwKv1rJtVliM75st5EwtxfDCX0n6yvJwsRk74gUPs8sGqXwGRiAHPqwUsqSP1WeAbMu2SEPUefFpe2jvfllamPGKjnwwyyFaV2FvLwMozHK/AF0sNPHh/6hot2mPiB8LNMNIPTu/M38le21IHqOceMl3CXnow2MCRljB7XpY1hvxTlCEUgkPz6S8q7ly3rAf+3F21wacqeBeQ4LBijGQ==', 'payment_config_status' => 'active'],
            ['name' => 'Mpesa_PassKey', 'value' => 'bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919', 'payment_config_status' => 'active'],
            ['name' => 'Mpesa_ClientCredentials', 'value' => '{"access_token": "GRSKXCn5gZTZZLVyKnbgy8j6O2ON","expires_in": "3599"}', 'payment_config_status' => 'active'],
            ['name' => 'Rave_EndPoint', 'value' => 'https://ravesandboxapi.flutterwave.com/flwv3-pug/getpaidx/api/v2/hosted/pay', 'payment_config_status' => 'active'],
            ['name' => 'Rave_PublicKey', 'value' => 'FLWPUBK-5313c360ecc6b2ac9c4037c8469bb0b3-X', 'payment_config_status' => 'active'],
            ['name' => 'Rave_PrivateKey', 'value' => 'FLWSECK-d4ddbe3ca88b993de69cbca417b468ad-X', 'payment_config_status' => 'active'],
            ['name' => 'Rave_Callback', 'value' => \App\Helpers\LP::appURL() . '/api/v1/rave/callback', 'payment_config_status' => 'active'],
            ['name' => 'CountryCode', 'value' => '254', 'payment_config_status' => 'active']
        );
        // Loop through each PaymentConfig above and create the record for them in the database
        foreach ($PaymentConfigs as $PaymentConfig) {
            PaymentConfig::create($PaymentConfig);
        }
        
        PaymentConfig::reguard();
    }
}
