<?php

use App\PaymentMethod;
use Illuminate\Database\Seeder;

class PaymentMethodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        PaymentMethod::unguard();

        DB::table('lp_payment_method')->delete();

        $PaymentMethods = array(
            ['name' => 'm-pesa', 'account_number' => 600000],
            ['name' => 'card', 'account_number' => 11900578]
        );
        // Loop through each PaymentMethod above and create the record for them in the database
        foreach ($PaymentMethods as $PaymentMethod) {
            PaymentMethod::create($PaymentMethod);
        }
        
        PaymentMethod::reguard();
    }
}
