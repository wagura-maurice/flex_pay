<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLpPaymentCurrencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lp_payment_currency', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('currency_name')->unique();
            $table->string('currency_code')->unique();
            $table->float('exchange_rate', 32); // to be update every 5 hrs
            $table->enum('payment_currency_status', ['active', 'inactive'])->default('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lp_payment_currency');
    }
}
