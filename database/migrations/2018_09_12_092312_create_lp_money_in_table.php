<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLpMoneyInTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lp_money_in', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('names')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->float('transaction_amount', 32);
            $table->string('transaction_code');
            $table->timestamp('transaction_date');
            $table->string('booking_reference')->nullable(); // if not null, direcly pay for booking / goal
            $table->bigInteger('payment_method_id');
            $table->longtext('payment_details')->nullable();
            $table->string('request_id')->nullable();
            $table->enum('money_in_status', ['processing', 'success', 'failed'])->default('processing');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lp_money_in');
    }
}
