<p align="center"><img src="https://www.flexpay.co.ke/img/flexpay-logo.b682e7cd.svg"></p>

## Getting Started with this Payments Package.

Our Aggregator for processing Mobile Money collections is called M-pesa, please follow the steps below to get started:

- [M-Pesa Developers](https://developer.safaricom.co.ke/docs).

Our Aggregator for processing Card collections is called Rave, please follow the steps below to get started:

- [Flutter Wave (Rave) Developers](https://flutterwavedevelopers.readme.io/v1.0).


## Project Sponsors.

We would like to extend our thanks to the following sponsors for helping fund on-going development:

- **[Kenyatta University](http://www.ku.ac.ke)**
- **[Chandaria-BIIC](http://www.ku.ac.ke/chandaria-biic)**
- **[Tuskys Kenya](https://tuskys.com)**
- **[Google App Engine](https://cloud.google.com/appengine)**
- **[Amazon AWS](https://aws.amazon.com)**
- **[FlexPay KE](https://flexpay.co.ke)**

If you are interested in becoming a sponsor, please feel free to [Get in Touch](mailto:business@waguramaurice.co.ke) with us.

## Contributing

Thank you for considering contributing to this Payment Pagackage! Get in touch with:

- **[Wagura Maurice](https://waguramaurice.co.ke)**

## Security Vulnerabilities

If you discover a security vulnerability within Flex Rave, please send an e-mail to Wagura Maurice via [wagura465@gmail.com](mailto:wagura465@gmail.com). All security vulnerabilities will be promptly addressed.

## License

The Payment Package is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
